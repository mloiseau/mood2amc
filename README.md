# moodle to amc

Convertir des [tests moodle](https://docs.moodle.org/3x/fr/Types_de_questions) en quizz [AMC](https://www.auto-multiple-choice.net/doc.fr), en partie au moins…

Ce module marche pour MA feuille de style AMC, qui contient :
```latex
\newif\if@English\@Englishfalse
\newcommand{\anglais}{\@Englishtrue} % pour afficher la version anglaise
%Traductions
\newcommand{\multiling}[2]{%
	\if@English%
	\selectlanguage{english}#2\selectlanguage{french}%
	\else%
	#1%
	\fi%
}
%TODO Ce serait mieux que tout soit dans la même commande
%La version avec et la version sans babel
\newcommand{\mling}[2]{%
	\if@English%
	#2%
	\else%
	#1%
	\fi%
}
```
et où ``{mlang en}`` et ``{mlang other}`` sont utilisés dans moodle.

Mettre ``unsafe`` à ``False`` pour ne pas remplacer les ``<span>`` (`utils.py`)

On peut fournir un un modèle LaTeX de sujet contenant ```%%%Exam content%%%``` à l'endroit où mettre le contenu de l'exam

```bash
usage: mood2amc.py [-h] [-m MOODLEDB] [-u] [-d DESTINATION] [-s SUB_DIR]

Convertir un export de base de questions moodle en sujet AMC
ex :
‣./mood2amc.py -m base-de-questions_moodle.xml
‣./mood2amc.py -u -m data/base-de-questions_moodle.xml -d data -s images
‣./mood2amc.py -m bdm.xml -d /home/user/Documents/Exams/mon_super_exam

optional arguments:
  -h, --help            show this help message and exit
  -m MOODLEDB, --moodledb MOODLEDB
                        la base de questions moodle xml à convertir
  -u, --unsafe          convertir les <span> monospace en listings (dangereux)
  -d DESTINATION, --destination DESTINATION
                        dossier de destination
  -s SUB_DIR, --sub_dir SUB_DIR
                        Dossier où mettre les images et listings (sous-dossier de destination)
```
