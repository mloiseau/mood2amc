#!/usr/bin/env python3
from re import compile,sub,escape,findall,DOTALL
from html import unescape
from urllib.parse import unquote

def process_listings(txt, folder,subf, q):
	txt = txt.replace("\r\n","\n")
	listings = findall(r'\<pre\>(.*)\</pre\>', txt, DOTALL)
	i=0
	for l in listings:
		i+=1
		filename = folder+subf+q+str(i)+".txt"
		f = open(filename, "w")
		f.write(unescape(l))
		txt = txt.replace("<pre>"+l+"</pre>","\t\t\\lstinputlisting[language=Python]{"+filename.replace(folder,"")+"}")
	return txt

def preprocess_images(txt, folder, subf, q):
	#"$£a1@ø" inserted by regexp, to be replaced by variable (dirty)
	txt = sub(r'<img src="[^/]*/([^"]*)" (alt="([^"]*)")?[^>]*>', r"\\\\\\includegraphics[width=0.8\\linewidth]{$£a1@ø\1}",txt).replace("$£a1@ø", subf)
	return txt

def remove_moodle_cdata(txt, folder, subf, unsafe, q):
	txt = process_listings(txt.replace("<![CDATA[", "").replace("]]>", "").replace("<strong>", "\\emph{").replace("</strong>", "}").replace("<em>", "\\emph{").replace("</em>", "}"),
			folder, subf, q)
	txt = preprocess_images(txt, folder, subf, q)
	if unsafe:
		txt = txt.replace('<span style="font:monospace">',"\lstinline[language=python]|").replace('<span style="font-family:monospace">',"\lstinline[language=python]|").replace('<span style="font-family=monospace">',"\lstinline[language=python]|").replace("</span>","|")
	return txt

def strip_tags(txt, folder, subf, unsafe, q="q"):
	return unescape(sub(compile('<.*?>'), '',remove_moodle_cdata(unquote(txt),folder,subf, unsafe, q)))

def mlang_2_multiling(txt, which = "both"):
	if which == "fr":
		res = sub(r"\{mlang\s*en\}([^\{]*?)\{mlang\}\s*((\{mlang\s*other\})|(\{mlang\s*fr\})|(\{mlang\}))([^\{]*)\{mlang\}", r"\6",txt)
	elif which == "en":
		res = sub(r"\{mlang\s*en\}([^\{]*?)\{mlang\}\s*((\{mlang\s*other\})|(\{mlang\s*fr\})|(\{mlang\}))([^\{]*)\{mlang\}",r"\1", txt)
	else:
		res = sub(r"\{mlang\s*en\}([^\{]*?)\{mlang\}\s*((\{mlang\s*other\})|(\{mlang\s*fr\})|(\{mlang\}))([^\{]*)\{mlang\}", r"\\multiling{\6}{\1}",txt)
	return res

def score_2_str(v):
	if int(v)==float(v):
		res = str(int(v))
	else:
		res = str(round(float(v),3))
	return res
