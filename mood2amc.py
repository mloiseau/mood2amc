#!/usr/bin/env python3
import argparse
from argparse import RawTextHelpFormatter #pour le formattage de l'aide
#déclaration des arguments
parser = argparse.ArgumentParser(formatter_class=RawTextHelpFormatter,description="""Convertir un export de base de questions moodle en sujet AMC
\033[1m\033[32mex :\033[0m
‣\033[0m\033[32m./mood2amc.py -m base-de-questions_moodle.xml\033[0m
‣\033[0m\033[32m./mood2amc.py -u -m data/base-de-questions_moodle.xml -d data -s images\033[0m
‣\033[0m\033[32m./mood2amc.py -m bdm.xml -d /home/user/Documents/Exams/mon_super_exam \033[0m""")
parser.add_argument("-m","--moodledb", help="la base de questions moodle xml à convertir", type=str, default=None)
parser.add_argument("-u","--unsafe", help="convertir les <span> monospace en listings (dangereux)", action="store_true")
parser.add_argument("-d","--destination", help="dossier de destination", type=str, default="data")
parser.add_argument("-s","--sub_dir", help="Dossier où mettre les images et listings (sous-dossier de destination)", type=str, default="images")

config = vars(parser.parse_args())
from os import path, makedirs
if not path.isdir(config["destination"]):
	makedirs(config["destination"],0o755)
if config["destination"][-1] != "/":
	config["destination"]+="/"
if config["sub_dir"][-1] != "/":
	config["sub_dir"]+="/"
if not path.isdir(config["destination"]+config["sub_dir"]):
	makedirs(config["destination"]+config["sub_dir"],0o755)

from XML_Moodle import Quizz
Quizz(config["moodledb"], config["destination"], config["sub_dir"], config["unsafe"]).save()
